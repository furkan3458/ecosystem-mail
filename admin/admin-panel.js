window.addEventListener("resize",setScroll);

window.onload = function(){
	setScroll();
}

function setScroll(){
	var height = window.innerHeight;
	document.getElementsByClassName("ta-con")[0].style.height = height - 168 + "px";
}

var e = document.querySelectorAll('[contenteditable="true"]');
for(var i = 0; i<e.length; i++){
	e[i].addEventListener('keydown', function(e) {
  		if (e.keyCode == 13) {    // if Enter has been pressed
    		e.preventDefault();
  		}
	});
}