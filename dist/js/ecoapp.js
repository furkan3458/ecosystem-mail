import {findCssClassStyle} from './functions.js';
import $ from 'jquery';
import 'bootstrap';

var ql_sub;
var ql_mess;

window.addEventListener("scroll",setNewMailScroll);

window.onload = function(){
	
	var ch_nm = document.getElementsByClassName("mail-input");
	for(var i = 0; i<ch_nm.length;i++){
		ch_nm[i].addEventListener("click",function(){
				if(this.getAttribute("aria-checked") == "false")
					this.setAttribute("aria-checked","true");
				else
					this.setAttribute("aria-checked","false");
			});
	}
	
	setNewMailPage();
	setNewMailScroll();
	setNewMailSendButton();
	setRecipientOptions();
}

function setNewMailPage(){
	
if(!document.getElementById("message"))
	return;
	
var Font = Quill.import('formats/font');
Font.whitelist = ['arial','arial-black','times-new-roman','nunito-bold', 'nunito-regular','poppins-regular','poppins-bold'];
Quill.register(Font, true);
var quill = new Quill('#edit1', {
	theme : 'bubble',
  placeholder:'Subject'
});

var quill2 = new Quill('#edit2', {
 	modules: {
          toolbar: '#toolbar-container',
          "emoji-toolbar": true,
          "emoji-shortname": true,
          "emoji-textarea": false,
		  imageResize: {displaySize: true,},
		  history: {delay: 500,maxStack: 500}
        },
  theme:'snow',
  placeholder:'Your Message'
});

var undo = document.querySelector('#undo');
undo.addEventListener('click', function() {
	quill2.history.undo();
});

var redo = document.querySelector('#redo');
redo.addEventListener('click', function() {
	quill2.history.redo();
});

var fullscreen = document.querySelector('#fullscr');
fullscreen.addEventListener('click',function(){
	var x = document.querySelector('.new-mail-container');
	
	if(this.getAttribute("aria-label")=="false"){
		x.classList.add('fullscreen');
		setFullScreen();
		window.addEventListener('resize',setFullScreen);
		this.setAttribute("aria-label","true")
	}else{
		x.classList.remove('fullscreen');
		window.removeEventListener('resize',setFullScreen);
		document.getElementById("edit2").removeAttribute("style");
		this.setAttribute("aria-label","false");
	}
});

$(function () {
  $('[tooltip]').tooltip()
})

ql_sub = quill;
ql_mess = quill2;
}

function setFullScreen(){
	var toolbar = document.getElementById("toolbar-container");
	var edit1 = document.getElementById("edit1");
	
	var x = findCssClassStyle("mail-options-cont","height");
	var y = findCssClassStyle("new-mail-recipients-cont","height");
	var k = parseInt(window.getComputedStyle(toolbar).getPropertyValue("height"));
	var l = parseInt(window.getComputedStyle(edit1).getPropertyValue("height"));
	
	var height = window.innerHeight;
	var edit2 = document.getElementById("edit2").style;
	edit2.minHeight = edit2.maxHeight = height - (x+y+k+l) +"px";
	
	
}

function setNewMailScroll(){
	if(document.getElementById("message")){
		
		var scrollSize = document.documentElement.scrollTop;
		document.body.style.overflowY = "auto";

		var topnavSize = findCssClassStyle("topnav","height");
		var mailcontSize = findCssClassStyle("new-mail-container","padding-top");
		var mailoptSize = findCssClassStyle("mail-options-cont","height");
		var mailrecSize = findCssClassStyle("new-mail-recipients-cont","height");
		
		var total = topnavSize + mailcontSize + mailoptSize + mailrecSize;
		var toolbar = document.getElementById("toolbar-container");
		
		
		if(scrollSize > total){
			
			var tolH = parseInt(window.getComputedStyle(toolbar).getPropertyValue("height"));
			document.getElementById("tl-cp").style.display = "block";
			document.getElementById("tl-cp").style.height = tolH+"px";
			
			toolbar.classList.add("toolbar-top")
		}
		else{
			document.getElementById("tl-cp").style.display = "none"; 
			toolbar.classList.remove("toolbar-top")
			var tb_buttons = toolbar.getElementsByTagName("button");
		}
	}
}

function setNewMailSendButton(){
	if(document.getElementById("send_button")){
		var send = document.getElementById("send_button");
		send.addEventListener("click",function(){
			var edit1 = document.getElementById("edit1");
			var edit2 = document.getElementById("edit2");
		
			var cont_subject = edit1.querySelector("#edit1 .ql-editor").innerHTML;
			var cont_message = edit2.querySelector("#edit2 .ql-editor").innerHTML;

			if(ql_sub.getLength() > 1 && ql_mess.getLength() > 1){
			
				console.log("Subject : "+ql_sub.getText()+"\nLength : "+ql_sub.getLength());
				console.log("Message : "+ql_mess.getText()+"\nLength : "+ql_mess.getLength());
				console.log("Content Subject : " + cont_subject);
				console.log("Content Message : " + cont_message);
			}
			else
				console.log("Can't Send");
			
			});
	}
}

function setRecipientOptions(){
	var selects = document.getElementsByName("reciver");
	for(var i = 0; i<selects.length; i++)
		selects[i].addEventListener("change",function(){
				if(this.value != "Select")
					this.classList.remove("rec-select");
				else
					this.classList.add("rec-select");
		});
}

