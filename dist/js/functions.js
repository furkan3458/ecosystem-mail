function collectionHas(a, b) { //helper function (see below)
    for(var i = 0, len = a.length; i < len; i ++) {
        if(a[i] == b) return true;
    }
    return false;
}
function findParentBySelector(elm, selector) {
    var all = document.querySelectorAll(selector);
    var cur = elm.parentNode;
    while(cur && !collectionHas(all, cur)) { //keep going up until you find a match
        cur = cur.parentNode; //go up
    }
    return cur; //will return null if not found
}

function findCssClassStyle(elm, style){
	//parseInt(window.getComputedStyle(popup[0]).getPropertyValue("padding-top"));
	var clas_name = document.getElementsByClassName(elm);
	
	if(clas_name == null)
		return 0;
		
	var comp = window.getComputedStyle(clas_name[0]).getPropertyValue(style);
	
	return parseInt(comp);
}
export default function functions(){
	console.log("Warning::Exported default function")
	return 0}

export{findParentBySelector,findCssClassStyle}