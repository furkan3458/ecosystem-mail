import {findParentBySelector} from './functions.js';
import VueI18n from 'vue-i18n'
import tr_lang from '../lang/ecolanguage-tr.json'
import en_lang from '../lang/ecolanguage-en.json'
import fr_lang from '../lang/ecolanguage-fr.json'
import es_lang from '../lang/ecolanguage-es.json'
import $ from 'jquery';
Vue.use(VueI18n)

const messages = {
  en: en_lang,
  tr: tr_lang,
  fr: fr_lang,
  es: es_lang
}

var lang_s = 'en'

if (typeof(Storage) !== "undefined") {
	if(localStorage.getItem("lang") != null)
		lang_s = localStorage.getItem("lang");
	else
		localStorage.setItem("lang",lang_s);
}

// Create VueI18n instance with options
const i18n_set = new VueI18n({
  locale: lang_s, // set locale
  messages
})

var vm = new Vue({
	el :'#app',
	i18n :i18n_set,
	data :{	en_lang,
			change_lang:lang_s,
			options:[
				{ text: i18n_set.messages[lang_s]["langs"]["english"], value: 'en' },
				{ text: i18n_set.messages[lang_s]["langs"]["turkish"], value: 'tr' },
				{ text: i18n_set.messages[lang_s]["langs"]["french"], value: 'fr' },
				{ text: i18n_set.messages[lang_s]["langs"]["spanish"], value: 'es' }
			]
		},
	methods :{
		count :function(event){
			if(event){
				var attr = event.target.getAttribute("aria-checked")
				if(attr == 'true'){
					var r = event.target.getAttribute("data-rep")
					var msg = this.$i18n.messages[lang_s]["view_all"].replace("{view_counter}",r)
					event.target.innerHTML = msg
				}
				else{
					event.target.innerHTML = this.$i18n.messages[lang_s]["hide_all"];
				}
			}
			else
			{
				var btn = document.getElementsByClassName("view-reply-button");
				for(var i = 0; i<btn.length; i++){
					var reply = findParentBySelector(btn[i],".reply-container")
					if(reply != null){
						var replies = reply.querySelectorAll(".reply-container .reply-container")
						if(btn[i].getAttribute("aria-checked") == "false"){
							var r = replies.length;
							var msg = this.$i18n.messages[lang_s]["view_all"].replace("{view_counter}",r)
							btn[i].setAttribute("data-rep",r)
							btn[i].innerHTML = msg
						}
						else
							btn[i].innerHTML = this.$i18n.messages[lang_s]["hide_all"];
					}
					else{console.log("Error - Parent Not Found")}
				}
			}
		},
		save: function(event){
			if(event){
				if(this.change_lang != null){
					this.$i18n.locale = this.change_lang;
					localStorage.setItem("lang",this.change_lang);
					console.log("Lang:"+this.change_lang);
				}
			}
		}
	}
})

vm.count()

function ipLookUp () {
  $.ajax('https://ipinfo.io/json')
  .then(
      function success(response) {
          console.log('User\'s Location Data is ', response);
          console.log('User\'s Country', response.country);
      },

      function fail(data, status) {
          console.log('Request failed.  Returned status of',
                      status);
      }
  );
}
ipLookUp()

/*document.getElementsByClassName("welcome-button")[0].addEventListener("click",function(){
	i18n.locale  = 'tr' 
	localStorage.setItem("lang",'tr');
});*/