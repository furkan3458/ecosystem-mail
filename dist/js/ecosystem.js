import {findCssClassStyle,findParentBySelector} from './functions.js';

var local_selected = 0;

window.addEventListener("resize",setScrollSize);

window.onload = function(){
	var mails = document.querySelectorAll('[role="mail-button"]');
	for(var i = 0; i<mails.length; i++){
		mails[i].addEventListener("click",clickMail);
	}
	
	setReplyButtons();
	setViewReplyButtons();
	setStepSelectChecks();
	setScrollSize();
	setReplies();
	setCheckboxes();
	setNuChiButtons();
}

function setReplyButtons(){
var rep_buttons = document.getElementsByClassName("reply-button-reply");
	for(var i = 0; i<rep_buttons.length; i++){
		rep_buttons[i].addEventListener("click",function(){
			var reply = findParentBySelector(this, ".reply-container");
			var replies = reply.querySelectorAll(".reply-container .reply-form-container");
			if(this.getAttribute("aria-checked") == "false"){
				for(var k = 0; k< replies.length; k++){
					replies[k].style.display = "block";
				}
				this.setAttribute("aria-checked","true");
			}
			else{
				for(var k = 0; k< replies.length; k++){
					replies[k].style.display = "none";
				}
				this.setAttribute("aria-checked","false");
			}
		});
	}
}

function setViewReplyButtons(){
	var view_rep_buttons = document.getElementsByClassName("view-reply-button");
	for(var i = 0; i<view_rep_buttons.length; i++){
		view_rep_buttons[i].addEventListener("click",function(){
			//var reply = this.parentNode.parentNode.parentNode;
			var reply = findParentBySelector(this, ".reply-container");
			var replies = reply.querySelectorAll(".reply-container .reply-container");
			if(this.getAttribute("aria-checked") == "false"){
				for(var k = 0; k< replies.length; k++){
					replies[k].style.display = "flex";
				}
				this.setAttribute("aria-checked","true");
			}
			else{
				for(var k = 0; k< replies.length; k++){
					replies[k].style.display = "none";
				}
				this.setAttribute("aria-checked","false");
			}
		});
	}
}

function setStepSelectChecks(){
	var selects = document.getElementsByClassName("step-select");
	for(var i = 0; i<selects.length; i++)
		selects[i].addEventListener("change",function(){
				var parent = findParentBySelector(this,".flex-order");
				var param = parent.querySelectorAll(".flex-order .info-container div .token");
				if(param.length){
					if(this.value != "Select")
						param[0].classList.add("icon-check-mark");
					else
					param[0].classList.remove("icon-check-mark");
				}
		});
}

function setScrollSize()
{	
	var scr = document.getElementsByClassName("scroll-container");
	for(var i = 0; i<scr.length; i++)
	{
		var popup = document.getElementsByClassName("popup-inner");
		var ibx = document.getElementsByClassName("inbox-menu");
		var hr = document.getElementsByClassName("half-right-inner");
		
		var heightSize = window.innerHeight;
		var topnavSize = findCssClassStyle("topnav","height");

		if(scr[i].getAttribute('aria-labelledby') == "step-scroll"){
			var hrSize = findCssClassStyle("half-right-inner","padding-top");
			scr[i].style.height = heightSize - (topnavSize +(hrSize*2)) + "px";
		}
			
		else if(scr[i].getAttribute('aria-labelledby') == "mail-scroll"){
			var popupSize = findCssClassStyle("popup-inner","padding-top");
			var ibxSize = findCssClassStyle("inbox-menu","height");
			scr[i].style.height = heightSize - (topnavSize +(popupSize*2)+ibxSize) + "px";
		}
		else if(scr[i].getAttribute('aria-labelledby') == "left-scroll"){
			scr[i].style.height = heightSize - (topnavSize) + "px";
		}
		
		else if(scr[i].getAttribute('aria-labelledby') == "mailbox-scroll"){
			var popupSize = findCssClassStyle("popup-inner","padding-top");
			scr[i].style.height = heightSize - (topnavSize +(popupSize*2)) + "px";
		}
	}	
}

function setReplies(){
	var rep = document.getElementsByClassName("reply-container");
	
	for(var i = 0; i<rep.length; i++)
	{
		var x = rep[i].querySelectorAll(".reply-container .reply-container");
		console.log("Reply : "+x+ " Reply Length : "+x.length);
		if(x.length >= 1){
			for(var k = 0; k< x.length; k++){
				x[k].style.display = "none";
			}
		}
		else{
			var show_button = rep[i].getElementsByClassName("view-reply-button");
			show_button[0].style.display = "none";
		}
		
		var reply_form = rep[i].getElementsByClassName("reply-form-container");
			reply_form[0].style.display = "none";
	}
}

function clickMail(){
	console.log(this.getAttribute("aria-labelledby"));
	window.location.replace("mailbox.html");
	//Mail açmak için
}

function clickCheckbox(){ //SELECT ALL MAIL
	var checks = document.getElementsByClassName("checkbox");
	var mail = document.getElementsByClassName("inbox-mail");
	var x = document.querySelector('[aria-labelledby="check_all"]');
	
	if(x.getAttribute("aria-checked") == "false")
	{
		x.setAttribute("aria-checked","true");
		local_selected = mail.length;
		for(var i = 0; i<mail.length; i++)
			mail[i].style.backgroundColor  = '#E4F5FF';
	}
	else
	{
		x.setAttribute("aria-checked","false");
		local_selected = 0;
		for(var i = 0; i<mail.length; i++)
			mail[i].style.backgroundColor  = 'white';
	}
	for(var i = 0; i<checks.length; i++)
	{
		if(x.getAttribute("aria-checked") == "false" )
		{
			checks[i].setAttribute("aria-checked","false");
		}
		else
		{
			checks[i].setAttribute("aria-checked","true");
		}
	}
	printSelected();
}

function setCheckboxes(){
	var checks = document.getElementsByClassName("checkbox");
	for(var i = 0; i<checks.length; i++)
	{
		if(checks[i].getAttribute("aria-labelledby") == "check_all")
			checks[i].addEventListener("click",clickCheckbox);
		else
			checks[i].addEventListener("click",function()
			{
				if(this.getAttribute("aria-checked") == "false")
				{
					this.setAttribute("aria-checked","true");
					var par = findParentBySelector(this,'.inbox-mail');
					par.style.backgroundColor = '#E4F5FF';
					local_selected = local_selected + 1;	
				}
				else
				{
					this.setAttribute("aria-checked","false");
					var par = findParentBySelector(this,'.inbox-mail');
					par.style.backgroundColor = 'white';
					local_selected = local_selected - 1;
				}
				printSelected();
				event.cancelBubble=true;
			});
	}
}

function printSelected(){
	
	var selected_text = document.getElementById("selected_mail");
	
	if(local_selected == 0)
		selected_text.style.visibility = "hidden";
	else
		selected_text.style.visibility = "visible";
			
	selected_text.innerHTML = local_selected + " Selected";
}

function setNuChiButtons(){
	var nu = document.getElementById("nu-but");
	var chi = document.getElementById("chi-but");
	var mi = document.getElementsByClassName("mi");
	
	if(nu || chi){
		nu.addEventListener("click",function(){
			mi[0].style.width = "200px";
		});
	
		chi.addEventListener("click",function(){
			mi[0].style.width = 0;
		});
	}
}
